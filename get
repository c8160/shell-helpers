#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-only
set -euo pipefail

# Parsed CLI arguments
declare -A args=(
    [raw_repo]=""
    [force]=false
    [print]=""
)
# Base path to checkout repos underneath
BASE_PATH="$(readlink -f "$HOME")/repos"
# Suffix to strip from git URLs
DOTGIT_SUFFIX=".git"
# List of git forges to prepend to shortened repo URL
declare -a git_prefixes=("https://github.com" "https://gitlab.com")

# Log message at "info" severity
function _info { echo -e "\e[34mINFO\e[0m  $*"; }
# Log message at "warning" severity
function _warn { echo -e "\e[33mWARN\e[0m  $*"; }
# Log message at "ok" severity
function _ok { echo -e "\e[32m OK \e[0m  $*"; }
# Log message at "error" severity
function _err { echo -e "\e[31mERRO\e[0m  $*" 1>&2; }
# Log message at "error" severity and terminate execution. For this to terminate execution, the
# calling shell must set the `-e` bash option (errexit). Adds information to track callers if setup
# correctly (See `_caller` below).
function _fatal {
    _err "$@"
    exit 1
}

function _usage {
    cat << EOF
Usage: $0 [OPTIONS] GIT_URL

Options:
  -h, --help            Print this help text
  -f, --force           Delete leftover non-git directories at the destination,
                        e.g. from a previous, aborted clone
  -p, --print FILE      Print the target directory to FILE after successful
                        execution, as a single line of text without a trailing
                        newline. FILE is truncated in the process. Useful for
                        parsing the output in downstream tooling.

Clone a git repo to the local 'repos' dir. The remote path/URL is preserved to
create a hierarchical structure beneath 'repos' that resembles the repos
location on the git server. Refer to the examples below for additional
information.

This script accepts git URLs in one of three forms:

 1. Fully-qualified HTTP(S) without auth: 'http(s)://\$GIT_HOST/\$REPO_PATH'
 2. Fully-qualified SSH: '\$GIT_USER@\$GIT_HOST:\$REPO_PATH'
 3. Short-URLs: '\$REPO_PATH'

Short-URLs are resolved by prepending a selection of well-known git servers
until one answers for the given repo. Note that this attempts to get the repo
without authentication. If your repo is private or requires auth to contact,
please use one of the fully-qualified URL forms instead.

Well-known git servers currently supported:
$(for p in "${git_prefixes[@]}"; do echo "  - $p"; done)

Example:
  - Input: 'https://github.com/zellij-org/zellij'
       or: 'git@github.com:zellij-org/zellij'
       or: 'zellij-org/zellij'
  - Cloned to: '\$HOME/repos/github.com/zellij-org/zellij'
EOF
}

[[ $# -gt 0 ]] || {
    _usage 1>&2
    _fatal "you must provide at least one argument"
}
command -v "git" &> /dev/null ||
    _fatal "this script requires 'git' to function correctly"

while [[ $# -gt 0 ]]; do
    case "$1" in
        "-h" | "--help")
            _usage
            exit 0
            ;;
        "-f" | "--force")
            args[force]=true
            shift
            ;;
        "-p" | "--print")
            [[ $# -ge 2 ]] ||
                _fatal "'$1' requires argument 'FILE'"
            args[print]="$(readlink -f "$2" ||
                _fatal "'$2' cannot be resolved to an absolute path")"
            shift 2
            ;;
        -*)
            _fatal "unknown CLI options '$1'"
            ;;
        *)
            [[ -z "${args[raw_repo]}" ]] ||
                _fatal "you must provide only a single git repo"
            args[raw_repo]="$1"
            shift
            ;;
    esac
done

# Disassemble the repo argument
declare -A git_repo=(
    # scheme of the repo argument, if any
    [scheme]=""
    # authentication info, if any
    [auth]=""
    # host to connect to
    [host]=""
    # path to the repo
    [path]=""
    # fully resolved URL
    [full_url]=""
)

if [[ "${args[raw_repo]}" =~ ^(https?)://([-_.a-zA-Z0-9]+)/(.+)$ ]]; then
    git_repo[scheme]="${BASH_REMATCH[1]}"
    git_repo[host]="${BASH_REMATCH[2]}"
    git_repo[path]="${BASH_REMATCH[3]}"
    git_repo[full_url]="${args[raw_repo]}"
elif [[ "${args[raw_repo]}" =~ ^([-_a-zA-Z0-9]+)@(.+):(.+)$ ]]; then
    git_repo[scheme]="ssh"
    git_repo[auth]="${BASH_REMATCH[1]}"
    git_repo[host]="${BASH_REMATCH[2]}"
    git_repo[path]="${BASH_REMATCH[3]}"
    git_repo[full_url]="${args[raw_repo]}"
elif [[ "${args[raw_repo]}" =~ ^([-_a-zA-Z0-9]+)/(.+)$ ]]; then
    # Try different git hosts to see if one fits
    git_repo[path]="${args[raw_repo]}"
    for prefix in "${git_prefixes[@]}"; do
        url="$prefix/${git_repo[path]}"
        # Set askpass to some bogus value so it doesn't try to perform
        # interactive auth. If this is a private repo, you'll have to provide
        # some URL that includes authentication instead.
        if git -c core.askPass="true" ls-remote --refs "$url" &> /dev/null; then
            git_repo[full_url]="$url"
            git_repo[host]="${prefix#http*://}"
            break
        else
            continue
        fi
    done
    if [[ -z "${git_repo[full_url]}" ]]; then
        err_msg="Failed to resolve short-url '${args[raw_repo]}' to a valid git repo URL."
        err_msg="$err_msg\nTried the following hosts:\n\n"
        for p in "${git_prefixes[@]}"; do
            err_msg="$err_msg  - '$p'\n"
        done
        _fatal "$err_msg"
    fi
else
    _fatal "failed to parse repo URL '${args[raw_repo]}'"
fi
git_repo[path]="${git_repo[path]%$DOTGIT_SUFFIX}"

target_dir="$BASE_PATH/${git_repo[host]}/${git_repo[path]}"
# Print the target directory
function _print_target_dir {
    if [[ -n "${args[print]}" ]]; then
        echo -n "$target_dir" > "${args[print]}"
    fi
}

if [[ -d "$target_dir" ]]; then
    # Dir exists locally on disk
    pushd "$target_dir" &> /dev/null
    if git rev-parse --show-toplevel &> /dev/null; then
        _info "git repo already checked out at '$target_dir'"
        _print_target_dir
        exit 0
    else
        # Maybe it's an empty dir?
        dirsize="$(stat -c "%s" "$PWD")"
        if [[ "$dirsize" -eq 0 ]]; then
            if "${args[force]}"; then
                popd &> /dev/null
                rm -rf "$target_dir"
            else
                _fatal "found non-empty, non-git dir at '$target_dir', refusing to continue"
            fi
        fi
    fi
else
    _info "creating target dir '$target_dir'"
    mkdir -p "$target_dir"
fi

# Perform the clone/checkout
git clone "${git_repo[full_url]}" "$target_dir"
_print_target_dir
