# Shell Helper Scripts

A wild collection of wrappers and shell scripts I use frequently.

This project started out as a bunch of wrappers around podman containers for
`pandoc` and `latex`, but it is growing steadily ever since. It mostly includes
wrappers to run CLI tools from containers as if they were installed natively.
But there are some other utilities in here that I find handy from time to time
(such as `256colors`).


## How do I use it?

For the maximum convenience, install the tools somewhere into your `$PATH`. The
`link_stuff.sh` script in this repository for example creates symlinks inside
`~/.local/bin` that have the same names as the wrappers here and point to these
wrappers. This way you can update the wrappers (by e.g. pulling this
repository) and your changes are instantly applied.

Otherwise, you can of course pick individual wrappers and place them wherever
you want. Be aware that most of these wrappers rely on `pmhelp` from this
repository, so don't forget to put that on your `$PATH`, too.

Once you have the wrappers on your path, just call them! Most of the wrappers
are designed such that they replace the CLI application they are named after.

**In any case** I suggest you have a look at the wrappers contents first. Some
of them (such as `bees`) I have adapted to use default arguments or additional
mounts that you may not have or want.


## Particularly interesting commands you find here

This section highlights some commands that don't wrap identically named CLI
applications:

- `pmhelp`: Convenience function that provides shorthands for some
  commonly-used podman arguments
- `fedora`: Spawns a fedora container for sandboxed experimenting and mounts a
  persistent volume to the `dnf` cache to speed up application installation
- `md2pdf`: Takes as argument a Markdown file and converts it to PDF via
  `pandoc` and `latexmk`
- `vw`: Alias for `nvim $(which ARGUMENT)` to quickly edit scripts on your
  `$PATH`
- `256colors` prints the 256 "default" terminal colors (useful for theming
  things)


## Pitfalls and Gotchas

- When installing somewhere to your users `$PATH`, the wrappers aren't visible
  to root: You can't just `sudo bees`. You can work around this in one of two
  ways:
    - Install/link the wrappers to your system `$PATH`, or
    - call them as `sudo env "PATH=$PATH" $WRAPPER`, since this will use your
      users `$PATH` when running the command as root.
- Most of the container wrappers will print a "hint" to you that looks like
  this:
  ```
   ****************************************
    THIS COMMAND IS RUNNING IN A CONTAINER
   ****************************************
  ```
  It is printed to `stderr`, so it doesn't interfere with the usual CLI parsing
  (such as `grep`). This is to remind you that the command you are executing is
  actually a container. This is often helpful in debugging situations such as
  "Why didn't `X` pick up these changes?" --> Probably because it was running
  as container.
- Keeping your application containers updated can become bothersome. I highly
  recommend [`topgrade`](https://github.com/r-darwish/topgrade) to automate
  this.
- Remember to regularly run `podman image prune -f` to remove containers that
  are obsolete because they have been upgraded. Podman will not remove these
  containers automatically.
- Your container storage under `~/.local/share/containers/storage/overlay`
  (where container images are stored) will likely become rather large. You may
  want to consider excluding it from your backups (Since the containers are
  versioned anyway and your persistent data is stored somewhere else) and it is
  likely a good candidate for filesystem deduplication.


## Why not install things natively?

That's a question everyone has to answer for themselves. My personal point of
view:

- I find myself working across many heterogeneous systems running different
  OSes etc. The only thing they have in common is `podman`,
- These applications run **anywhere** you have access to podman. Since podman
  containers can be run by users, you don't need admin privileges to use any of
  these containers and hence applications,
- I use immutable OSes on almost all of my devices (Fedora Silverblue and
  friends) and I like to keep my OS as close to stock as I can,
- I like the extra sandboxing that containers give me, i.e. some random CLI
  application won't spit dozens of files to secret locations in my rootfs
  (Hello python!), I can keep programs from accessing the internet, my home
  folder, etc.,
- When one day I don't need application `X` any longer, I delete the container
  and maybe a container volume and its *gone*,
- I have full control over the application versions I run, this makes me 100%
  independent of what the underlying OS ships/does,
- I can even switch application versions *while the system is running*, no need
  to reboot etc.,
- I *really* like containers.

If you don't agree with any of these opinions that's fine, it's my *personal*
point of view after all.

