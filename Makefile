# General Make-sanity
.ONESHELL:
SHELL = /usr/bin/env
.SHELLFLAGS = bash -euo pipefail -c

BIN_PATH := ${HOME}/.local/bin
LINK_CMD := ln -sri

# Container-wrappers
BINS := ansible ansible-galaxy ansible-playbook ansible-vault bees below biber chktex glances \
        julia latexindent nitropy pandoc pv trivy ykman
# LSPs
BINS += ansible-language-server bash-language-server ltex-ls ltex-ls-plus lua-language-server \
        marksman tectonic texlab
# Custom tools
BINS += 256colors fedora get gitlab-merge gitlab-sync gn8 md2pdf pmhelp pre-backup \
        semver-check vw whatsup zz
# IGNORED
#BINS += glab

.PHONY: default
default:
	@echo "no default target, try 'install' or 'uninstall'"

.PHONY: install
install: ${BINS}
	${LINK_CMD} ${BINS} ${BIN_PATH}/
	${LINK_CMD} magic.bash ${HOME}/.magic.bash

.PHONY: uninstall
uninstall:
	$(foreach bin,${BINS},rm -f ${BIN_PATH}/${bin}
	)
	rm -f ${HOME}/.magic.bash
