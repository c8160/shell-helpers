#!/usr/bin/env bash
#
# # Toys and tricks for programming in bash
#
# This is my personal collection of helpers, common functions, and cool hacks/tricks I discovered
# while working with bash. This script is meant to be sourced by other scripts, to provide
# library-like building blocks and functions to write better shell scripts with ease. Please do not
# source this script in an interactive session, as it will likely cause havoc there.

# source-guard (Only source this once)
[[ -z "${__MAGIC_SOURCED__:-""}" ]] && __MAGIC_SOURCED__=1 || return 0

# --------------------------------------------------------------------------------------------------
# LOGGING
#
# Provides a simple logging mechanism that prepends a severity in front of log messages.
# --------------------------------------------------------------------------------------------------

# Log message at "info" severity
function _info { echo -e "\e[34mINFO\e[0m  $*"; }
# Log message at "warning" severity
function _warn { echo -e "\e[33mWARN\e[0m  $*"; }
# Log message at "ok" severity
function _ok { echo -e "\e[32m OK \e[0m  $*"; }
# Log message at "error" severity
function _err { echo -e "\e[31mERRO\e[0m  $*"; }
# Log message at "error" severity and terminate execution. For this to terminate execution, the
# calling shell must set the `-e` bash option (errexit). Adds information to track callers if setup
# correctly (See `_caller` below).
function _fatal {
    _err_ctx print
    caller="$(_caller)"
    _err "$* $caller"
    exit 1
}

# --------------------------------------------------------------------------------------------------
# ERROR HANDLING AND TRACING
#
# Provide sophisticated error messages and caller tracing using shell builtins. This partially
# relies on the logging implemented above to present the messages in a readable manner. You
# interact with this exclusively through the `_err_ctx` function, for example like so:
#
# ```bash
# _err_ctx push "a piece of high-level context"
# _err_ctx push "a piece of lower-level context"
# [[ ... ]] || _fatal "this didn't work"
# ```
#
# Which would result in output such as:
#
# ```
# ERRO  this didn't work
# ERRO  a piece of lower-level context
# ERRO  a piece of high-level context
# ```
#
# To remove the most recently added piece of context, use `_err_ctx pop`.
#
# **IMPORTANT**: For this to work you must fulfill the following requirements:
#
# - The script sourcing this one must set the `-e` and `-E` bash options
# - You mustn't overwrite the `__ERROR_CTX` variable yourself
# - You mustn't overwrite the `SIGERR` trap
# --------------------------------------------------------------------------------------------------

# Storage for error context messages
declare -a __ERROR_CTX
# Decides if we track caller information
declare __ERROR_TRACK_CALLER=false

# Determine the caller of whoever called this function (2 stackframes up) and print it in the
# format `(calling_file@lineno)`.
#
# **IMPORTANT**: This only works when `__ERROR_TRACK_CALLER` has value "true"!
function _caller {
    if "${__ERROR_TRACK_CALLER:-false}"; then
        declare -a CALLER
        my_caller="$(caller 1)"
        readarray -d' ' -t CALLER <<< "$my_caller"
        echo "(${CALLER[2]%$'\n'}@${CALLER[0]})"
    fi
}

# Manipulate the error context.
#
# Provides three subcommands:
#
# - push: Store the given message as error context for later retrieval
# - pop: Pop the most recently stored error context and remove it
# - print: Called by the default `SIGERR` trap defined below
function _err_ctx {
    declare CMD
    declare -i i=0
    [[ "$#" -ge 1 ]] || _fatal "'_err_ctx' needs at least one arg"
    CMD="$1"
    shift 1
    case "$CMD" in
        "push")
            __ERROR_CTX+=("$* $(_caller)")
            ;;
        "pop")
            NERR="${#__ERROR_CTX[@]}"
            [[ "$NERR" -gt 0 ]] && unset "__ERROR_CTX[$((NERR - 1))]"
            ;;
        "print")
            # check if context empty
            [[ -n "${__ERROR_CTX[*]:-""}" ]] || return 0
            NERR="${#__ERROR_CTX[@]}"
            [[ "$NERR" -gt 0 ]] || return 0
            for ((i = 0; i < ${#__ERROR_CTX[@]}; i++)); do
                _err "${__ERROR_CTX[$i]}"
            done
            ;;
        *)
            _err "unknown command '$CMD'"
            _fatal "did you mean/forget 'push' or 'pop'?"
            ;;
    esac
}
# Print all error contexts when an unhandled error occurs
trap "_err_ctx print" ERR

# --------------------------------------------------------------------------------------------------
# CURSOR MANIPULATION
# --------------------------------------------------------------------------------------------------

# Store cursor for subsequent use of `_rc`
function _sc { ! [[ -t 1 ]] || tput sc; }
# Restore cursor position stored with `_sc` and delete until end of screen
function _rc {
    ! [[ -t 1 ]] || tput rc
    echo -ne "\e[0J"
}
# Clear the most-recently printed line and set the cursor back there. Useful to overwrite the last
# output message.
function _clear { echo -ne "\e[1F\e[2K"; }

# --------------------------------------------------------------------------------------------------
# MISC
# --------------------------------------------------------------------------------------------------
function _exists {
    if [[ "$#" -eq 1 ]]; then
        command -v "$1" &> /dev/null
    else
        _fatal "you must supply an argument to check existence for"
    fi
}
