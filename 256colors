#!/usr/bin/env bash

declare -A args=(
    [base]=false
    [bright]=false
    [cube]=false
    [gray]=false
)

function _fatal {
    echo -e "\e[31mERROR\e[0m   $*" 1>&2
    exit 1
}

function _usage {
    cat << EOF
Usage: $0 [OPTION...]
Show the ANSI color codes.

Available Options:
  -h, --help            show this help text
  -s, --base            show the base color codes (0-7)
  -b, --bright          show the bright color codes (8-16)
  -c, --cube            show the rgb color cube (17-231)
  -g, --gray            show the grayscale color codes (232-255)
      --rgb             synonym to '--cube'

If no options are given, prints all color codes. The individual options can be
combined to show the respective colors at once.
EOF
}

if [[ $# -eq 0 ]]; then
    args[base]=true
    args[bright]=true
    args[cube]=true
    args[gray]=true
fi

while [[ $# -gt 0 ]]; do
    case "$1" in
        "-h" | "--help")
            _usage
            exit 0
            ;;
        "-s" | "--base")
            args[base]=true
            shift
            ;;
        "-b" | "--bright")
            args[bright]=true
            shift
            ;;
        "-c" | "--cube" | "--rgb")
            args[cube]=true
            shift
            ;;
        "-g" | "--gray")
            args[gray]=true
            shift
            ;;
        -*)
            opt="$1" && shift 1
            # Check for key-value argument pairs separated with '='
            if [[ "$opt" =~ ^(-[[:alnum:]]|--[[:alnum:]]+)=(.*)$ ]]; then
                set -- "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}" "$@"
            # Check for multiple short flags squeezed into one argument
            elif [[ "$opt" =~ ^-[[:alnum:]]{2,}$ ]]; then
                for idx in $(seq "${#opt}" -1 2); do
                    optchar="$(cut -c "$idx" <<< "$opt")"
                    set -- "-$optchar" "$@"
                done
            else
                _fatal "unknown option '$opt'"
            fi
            ;;
        *)
            _fatal "unknown keyword '$1'"
            ;;
    esac
done

function _title {
    echo -e "\e[4;1m$*\e[0m"
}

if "${args[base]}"; then
    _title "Base colors:"
    for base_color in $(seq 0 7); do
        fg_color=255
        if [[ "$base_color" -ge 1 ]]; then
            fg_color=232
        fi

        printf "\e[48;5;%d;38;5;%dm%6s" "$base_color" "$fg_color" "$base_color"
    done
    echo -e "\e[0m"
fi

if "${args[bright]}"; then
    _title "Bright base colors:"
    for bright_colors in $(seq 8 15); do
        fg_color=255
        if [[ "$bright_colors" -ge 9 ]]; then
            fg_color=232
        fi

        printf "\e[48;5;%d;38;5;%dm%6s" "$bright_colors" "$fg_color" "$bright_colors"
    done
    echo -e "\e[0m"
fi

if "${args[cube]}"; then
    _title "Color Cube:"
    declare -i slice_no=1
    for color_cube in $(seq 16 231); do
        if [[ $(((color_cube - 16) % 36)) -eq 0 ]]; then
            echo -e "\e[3m  > Slice $slice_no:\e[0m"
            slice_no+=1
        fi
        if [[ $(((color_cube - 16) % 6)) -eq 0 ]]; then
            echo -n "      "
        fi
        fg_color=255
        if [[ "$color_cube" -ge 20 ]]; then
            fg_color=232
        fi

        printf "\e[48;5;%d;38;5;%dm%6s" "$color_cube" "$fg_color" "$color_cube"

        if [[ $(((color_cube - 16) % 6)) -eq 5 ]]; then
            printf "\e[0m\n"
        fi
    done
fi

if "${args[gray]}"; then
    _title "Grayscales:"
    for grayscale in $(seq 232 255); do
        fg_color=255
        if [[ "$grayscale" -gt 243 ]]; then
            fg_color=232
        fi
        printf "\e[48;5;%d;38;5;%dm%6s" "$grayscale" "$fg_color" "$grayscale"

        if [[ $(((grayscale - 232) % 8)) -eq 7 ]]; then
            printf "\e[0m\n"
        fi
    done
fi
