#!/usr/bin/env bash
#
# A wrapper around 'vim $(which $1)'
if [[ $# -ne 1 ]]; then
    echo "Requires exactly one argument" 1>&2
    exit 1
fi

cmd="$(command -v "$1" || true)"
if [[ -z "$cmd" ]]; then
    echo "command '$1' doesn't exist" 1>&2
    exit 1
else
    cmd="$(readlink -f "$cmd")"
    if [[ -n "$EDITOR" ]]; then
        exec "$EDITOR" "$cmd"
    else
        vi "$cmd"
    fi
fi
