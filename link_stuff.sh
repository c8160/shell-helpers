#!/usr/bin/env bash
set -euo pipefail

#
# Create symlinks from all files in this dir to $TARGETDIR
#

# Files to exclude from linking
EXCLUDE=(link_stuff.sh README.md LICENSE magic.bash glab)

declare -A ARGS=(
    [remove]=false
)

function _usage {
    cat << EOF
Usage: $0 [OPTION]...
(Un-)link scripts from this directory to '\$PATH'.

Availabe options:
    -h, --help      show this help text
    -r, --remove    remove previously created symlinks
EOF
}

while [[ "$#" -gt 0 ]]; do
    case "$1" in
        "-h" | "--help")
            _usage
            exit 0
            ;;
        "-r" | "--remove")
            ARGS[remove]=true
            shift 1
            ;;
        *)
            echo "unknown argument '$1'" >&2
            exit 1
            ;;
    esac
done

if [[ $EUID -eq 0 ]]; then
    echo -e "\e[31m! Running as root. Linking system-wide !\e[0m"
    TARGETDIR="/usr/local/bin"
else
    TARGETDIR="$HOME/.local/bin"
fi
if ! [[ "$PATH" =~ :?$TARGETDIR:? ]]; then
    echo -e "\e[33mDefault directory '$TARGETDIR' is not in \$PATH\e[0m" 1>&2
fi

# Switch to script directory
scriptpath="$(readlink -f "$0")"
pushd "$(dirname "$scriptpath")" &> /dev/null ||
    {
        echo "failed to change into script directory" 1>&2
        exit 1
    }
for file in *; do
    if [[ "${EXCLUDE[*]}" =~ $file ]]; then
        # Skip this
        echo -e "\e[33mExcluding:\e[0m $file"
        continue
    fi

    srcfile="$PWD/$file"
    destfile="$TARGETDIR/$file"

    if "${ARGS[remove]}"; then
        if [[ -L "$destfile" ]]; then
            echo -e "\e[32m Removing:\e[0m $file"
            rm -f "$destfile"
        else
            echo -e "\e[33m  Warning:\e[0m not removing '$file' since it's not a symlink"
        fi
    else
        # Check if files are equal, skip if they are
        if [[ -f "$destfile" ]]; then
            echo -e "\e[34m Ignoring:\e[0m $file"
            continue
        else
            echo -e "\e[32m  Linking:\e[0m $file"
            ln -frs "$srcfile" "$destfile"
        fi
    fi
done

if "${ARGS[remove]}"; then
    rm -f "$HOME/.magic.bash"
elif ! [[ -e "$HOME/.magic.bash" ]]; then
    ln -sr "$PWD/magic.bash" "$HOME/.magic.bash"
fi
